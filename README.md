# HTML Flipbook Quran Utsmani

This flipbook of the Qur'an is created using turn.js and some javascript libraries. Page after page quran is compiled using the results of physical qur'an scanned photos.
You could visit the demo of the project on https://apps.nusagates.com/quran

<img src='https://gitlab.com/budairi/html-flipbook-quran-utsmani/raw/master/screenshot.png'/>

<h2>How To Use? </h2>
Just download <a href='https://gitlab.com/budairi/html-flipbook-quran-utsmani/-/archive/master/html-flipbook-quran-utsmani-master.zip'>this repository</a> and then open index.html on your browser. You could upload this project to your server so you could access this quran online.

<h2>Support</h2>
You could reach me via nusagates@gmail.com or https://forum.nusagates.com